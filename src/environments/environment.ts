// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  configFirebase: {
      apiKey: "AIzaSyAro3VJVDD9W-7VFHP_9M4dySRabU_dhYs",
      authDomain: "cybermenu-de56b.firebaseapp.com",
      databaseURL: "https://cybermenu-de56b.firebaseio.com",
      projectId: "cybermenu-de56b",
      storageBucket: "",
      messagingSenderId: "224291023202",
      appId: "1:224291023202:web:baf1fd407ba9c497"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
