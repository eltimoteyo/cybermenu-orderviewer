import { Component, ViewEncapsulation} from '@angular/core';
import { OrdersService } from '../shared/order.service';
import { CarteService } from '../shared/carte.service';
import { MatDialog } from '@angular/material';
import { DialogConfirmComponent } from './dialog-confirm/dialog-confirm.component';

export interface Saucer {
  saucerid: Number;
  saucerName: String;
  optSpecific:[];
  quantity:Number;
};
export interface Order {
  table: Number;
  items: Saucer[];
};

@Component({
  selector: 'carta',
  templateUrl: './carta.component.html',
  styleUrls: ['./carta.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CartaComponent {
spinerstate:boolean=false;
constructor(
  private orderService: OrdersService,
  private carteService: CarteService,
  public dialog: MatDialog
  ) { }
//constructor(private order:Order, private items:Saucer){};
carta:any=[
  {groupId: 1, groupName: 'Menu del dia', icon:'restaurant_menu', platos:[
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/shiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:{defaulOpt:'', options:[
      {idOption:12,nameOption:'sopa de pollo', descOption:'sola e pollo con fideos y papa'},
      {idOption:12,nameOption:'causa rellena', descOption:'causa relena con atun y cebolla'}
    ]}},
    {saucerId:1,saucerName:'Arroz con pollo',saucerImage:'https://material.angular.io/assets/img/shiba2.jpg',saucerDesc:'',quantityAdd:0, stock:'',price:'12.50', optSpecific:{defaulOpt:'', options:[
      {idOption:12,nameOption:'sopa de pollo', descOption:'sola e pollo con fideos y papa'},
      {idOption:12,nameOption:'causa rellena', descOption:'causa relena con atun y cebolla'}
    ]}},
    {saucerId:1,saucerName:'Pollo al vino',saucerImage:'https://material.angular.io/assets/img/shiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:{defaulOpt:'', options:[
      {idOption:12,nameOption:'sopa de pollo', descOption:'sola e pollo con fideos y papa'},
      {idOption:12,nameOption:'causa rellena', descOption:'causa relena con atun y cebolla'}
    ]}},
    {saucerId:1,saucerName:'Lomo saltado',saucerImage:'https://material.angular.io/assets/img/shiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:{defaulOpt:'', options:[
      {idOption:12,nameOption:'sopa de pollo', descOption:'sola e pollo con fideos y papa'},
      {idOption:12,nameOption:'causa rellena', descOption:'causa relena con atun y cebolla'}
    ]}},
    {saucerId:1,saucerName:'Estofado de pollo',saucerImage:'https://material.angular.io/assets/img/shiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:{defaulOpt:'', options:[
      {idOption:12,nameOption:'sopa de pollo', descOption:'sola e pollo con fideos y papa'},
      {idOption:12,nameOption:'causa rellena', descOption:'causa relena con atun y cebolla'}
    ]}},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/shiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:{defaulOpt:'', options:[
      {idOption:12,nameOption:'sopa de pollo', descOption:'sola e pollo con fideos y papa'},
      {idOption:12,nameOption:'causa rellena', descOption:'causa relena con atun y cebolla'}
    ]}},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/shiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:{defaulOpt:'', options:[
      {idOption:12,nameOption:'sopa de pollo', descOption:'sola e pollo con fideos y papa'},
      {idOption:12,nameOption:'causa rellena', descOption:'causa relena con atun y cebolla'}
    ]}}
   ]
  },
  {groupId: 2, groupName: 'Carta', icon:'restaurant', platos:[
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/shiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[]},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/shiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[]},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/shiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[]},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/shiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[]},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/shiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:{defaulOpt:'', options:[
      {idOption:12,nameOption:'sopa de pollo', descOption:'sola e pollo con fideos y papa'},
      {idOption:12,nameOption:'causa rellena', descOption:'causa relena con atun y cebolla'}
    ]}},
    {saucerId:1,saucerName:'Titulo Numero cuatro',saucerImage:'https://material.angular.io/assets/img/shiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[ ]}
   ]
  },
  {groupId: 3, groupName: 'Bebidas', icon:'local_bar', platos:[
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/shiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[]},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/hiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[]},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/hiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[]},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/hiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[]},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/hiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:{defaulOpt:'', options:[
      {idOption:12,nameOption:'sopa de pollo', descOption:'sola e pollo con fideos y papa'},
      {idOption:12,nameOption:'causa rellena', descOption:'causa relena con atun y cebolla'}
    ]}},
    {saucerId:1,saucerName:'Arroz con pollo',saucerImage:'https://material.angular.io/assets/img/hiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[ ]}
   ]
  },
  {groupId: 4, groupName: 'Postres', icon:'fastfood', platos:[
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/hiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[]},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/hiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[]},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/hiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[]},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/hiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[]},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/hiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:{defaulOpt:'', options:[
      {idOption:12,nameOption:'sopa de pollo', descOption:'sola e pollo con fideos y papa'},
      {idOption:12,nameOption:'causa rellena', descOption:'causa relena con atun y cebolla'}
    ]}},
    {saucerId:1,saucerName:'Titulo Numero uno',saucerImage:'https://material.angular.io/assets/img/hiba2.jpg',saucerDesc:'', quantityAdd:0, stock:'',price:'12.50', optSpecific:[ ]}
   ]
  }
];
totalItemsOrder = 0;
order=[];
orders;
//cantaddItems:any;
//itemAdd: any={saucerId: 1, saucerName:'Titulo Numero uno', idOption:12, quantity:2}
ngOnInit() {
  this.carteService.getCarte()
    .subscribe(carte => {
      console.log('estos son:',carte);
      this.orders = carte;
    });
}
 addItemToOrder(item, categoryName){
   console.log('start',item);
   var items = {
    saucerid: parseInt(item.saucerId),
    saucerName:item.saucerName,
    optSpecific:item.optSpecific.defaulOpt,
    status:'pending',
    category: categoryName,
    quantity:item.quantityAdd,
   };
   this.order.push(items);
   console.log(this.order);
   this.totalItemsOrder += items.quantity;
   //this.countItemOrder();
   return 0;
 }
 sendOrder(){
    this.orderService.myForm.value.items = this.order;
    let data = this.orderService.myForm.value;
    data.totalItemsOrder = this.totalItemsOrder;
    // call service 
    this.orderService.createOrder(data);
    this.totalItemsOrder = 0;
    this.order=[];
    this.openDialog();
    this.orderService.myForm.reset();
 }
 countItemOrder(){
  this.order.forEach(el => {
     this.totalItemsOrder += el.quantity;
   });
 }
 openDialog() {
    const dialogRef = this.dialog.open(DialogConfirmComponent, {
      width: '250px',
      data: {name: "eeeee", animal: "ggggg"}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed',result);
    });
}
 ayuda(){
   
 }
}
