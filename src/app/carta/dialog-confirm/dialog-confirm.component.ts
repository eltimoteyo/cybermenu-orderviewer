import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
    selector: 'dialog-confirm',
    templateUrl: 'dialog-confirm.component.html',
  })
  export class DialogConfirmComponent {
    constructor(
    public dialogRef: MatDialogRef<DialogConfirmComponent>, @Inject(MAT_DIALOG_DATA) 
    public data: any) {}
    onNoClick(): void {
        this.dialogRef.close();
      }
  }