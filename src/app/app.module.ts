import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { FormComponent } from './components/form/form.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatButtonToggle, MatButtonToggleModule, MatDialogModule } from '@angular/material';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { CartaComponent } from './carta/carta.component';
import { MaterialModule } from './material.module';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
// Firebase 
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ListItemsComponent } from './components/list-item/list-item.component';
import { Orderview } from './orderview/orderview.component';
import { OrderViewerComponent } from './orders/order-viewer/order-viewer.component';
import { GroupViewerComponent } from './orders/group-viewer/group-viewer.component';
import { ItemOrderComponent } from './orders/item-order/item-order.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { DialogConfirmComponent } from './carta/dialog-confirm/dialog-confirm.component';






@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FormComponent,
    MainMenuComponent,
    CartaComponent,
    Orderview,
    ListItemsComponent,
    OrderViewerComponent,
    GroupViewerComponent,
    ItemOrderComponent,
    DialogConfirmComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.configFirebase),
    AngularFirestoreModule,
    MatButtonToggleModule,
    FormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  exports: [
  ],
  entryComponents: [DialogConfirmComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
