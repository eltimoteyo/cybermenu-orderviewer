import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartaComponent } from './carta/carta.component';
import { OrderViewerComponent } from './orders/order-viewer/order-viewer.component';

const routes: Routes = [
  { path: '', component: CartaComponent }, 
  { path: 'viewer', component: OrderViewerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
