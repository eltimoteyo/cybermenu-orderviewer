import {Component, OnInit} from '@angular/core';
import { OrdersService } from 'src/app/shared/order.service';
  
  @Component({
    selector: 'order-viewer',
    styleUrls: ['./order-viewer.component.scss'],
    templateUrl: './order-viewer.component.html',
  })
  export class OrderViewerComponent implements OnInit  {
    constructor(private orderService: OrdersService) { }
    isLoading:boolean;
    orders:any;
    ordersPending=[];
    ordersProcessing=[];
    ordersReady=[];
    ngOnInit() {
      this.isLoading = true;
      this.orderService.getOrders()
        .subscribe(orders => {
          console.log('estos son:',orders);
          this.orders = orders;
          this.isLoading = false;
          this.getForSatus();
        });
        console.log('estos son:',this.ordersPending);
        console.log('estos son:',this.ordersProcessing);
        console.log('estos son:',this.ordersReady);
    }
    getForSatus(){
      this.orders.forEach(ord => {
        ord.items.forEach(el => {
          let order={
          table:ord.table,
          time:ord.time,
          category:el.category,
          optSpecific:el.optSpecific,
          quantity:el.quantity,
          saucerName:el.saucerName,
          status:el.status,
          saucerid:el.saucerid};
          if (el.status=='pending') {
            this.ordersPending.push(order);
          }else if (el.status=='processing') {
            this.ordersProcessing.push(order);
          }else{
            this.ordersReady.push(order);
          }
        });
      });
    }
    fruits = [
      'Apples',
      'Bananas',
      'Cherries',
      'Dewberries',
      'Blueberries',
      'Avocados',
    ];
    others = [
      'usb',
      'teclado',
      'monitor',
      'audifonos',
      'cables',
      'altavoz',
    ];
  }