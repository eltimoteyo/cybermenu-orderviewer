import { Component, AfterContentInit, ContentChildren, QueryList, HostListener } from '@angular/core';
import { FocusKeyManager } from '@angular/cdk/a11y';
import { DOWN_ARROW, UP_ARROW, LEFT_ARROW } from '@angular/cdk/keycodes';
import { ItemOrderComponent } from '../item-order/item-order.component';

@Component({
    selector: 'group-viewer',
    styleUrls: ['./group-viewer.component.scss'],
    templateUrl:'./group-viewer.component.html',
    host: { 'tabindex': '0' },
  })
  export class GroupViewerComponent implements AfterContentInit {
    @ContentChildren(ItemOrderComponent) 
    items: QueryList<ItemOrderComponent>;
    private keyManager: FocusKeyManager<ItemOrderComponent>;
  
    @HostListener('keydown', ['$event'])
    manage(event) {
      this.keyManager.onKeydown(event);
      if (this.keyManager) {
        if (event.keyCode === DOWN_ARROW || event.keyCode === UP_ARROW) {
          // passing the event to key manager so we get a change fired
         // this.keyManager.onKeydown(event);
          return false;
        } else if (event.keyCode === LEFT_ARROW) {
          // when we hit enter, the keyboardManager should call the selectItem method of the `ListItemComponent`
          //this.keyManager.activeItem.selectItem();
          console.log('a la derecha');
          return false;
        }
      }
    }
  
    ngAfterContentInit(): void {
      this.keyManager = new FocusKeyManager(this.items).withWrap();
    }
  }