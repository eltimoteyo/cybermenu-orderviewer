import { FocusableOption } from '@angular/cdk/a11y';
import { ElementRef, Input, Component } from '@angular/core';


@Component({
    selector: 'item-order',
    styleUrls: ['./item-order.component.scss'],
    host: { tabindex: '-1' },
    templateUrl:'./item-order.component.html',
  })
  export class ItemOrderComponent implements FocusableOption {
    @Input() 
    fruta: any;
    disabled: boolean;
  
    constructor(private element: ElementRef) {
    }
  
    getLabel(): string {
      return this.fruta;
    }
  
    focus(): void {
      this.element.nativeElement.focus();
    }
  }