import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { formatDate } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class CarteService {
  private carteCollection: AngularFirestoreCollection<any>;
  carte: Observable<any[]>;

  constructor(private readonly afs: AngularFirestore) {
    this.carteCollection = afs.collection<any>('dishes');
    this.carte = this.carteCollection.snapshotChanges().pipe(map(
      actions => actions.map(a => {
        const data = a.payload.doc.data() as any;
        const id = a.payload.doc.id;
        return { id, ...data };
      })
    ));
  }

  myForm = new FormGroup({
    table: new FormControl(localStorage.getItem("idmesacybermenu")),
    time: new FormControl(formatDate(new Date(), 'dd/MM/yyyy', 'en')),
    items: new FormControl(''),
    completed: new FormControl(false)
  });


  getCarte() {
    return this.carte;
  }
  updateCarte(item: any) {
    return this.carteCollection.doc(item.id).update(item);
  }
  deleteCarte(id: string) {
    return this.carteCollection.doc(id).delete();
  }
  createCarte(item: any) {
    return this.carteCollection.add(item);
  }
}